package com.ucab.airucab.controllers;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import com.ucab.airucab.views.ClientCreateView;
import com.ucab.airucab.views.DashboardView;

public class DashboardController implements ActionListener {
	
	public DashboardView vista;
	
	
	public DashboardController(){
		this.vista= new DashboardView();
		this.initializerView();
	}
	private void initializerView( ) {

		this.vista = new DashboardView();
		
		this.vista.btnAviones.addActionListener(this);
		this.vista.btnClientes.addActionListener(this);
		this.vista.btnProveedores.addActionListener(this);
		this.vista.btnEnvios.addActionListener(this);
		this.vista.btnPruebas.addActionListener(this);	
		this.vista.btnMateriales.addActionListener(this);
		this.vista.btnPedidos.addActionListener(this);
		
		this.vista.btnCerrarSesion.addActionListener(this);
		this.vista.FrInicio.setVisible(true);
	}
	public void actionPerformed(ActionEvent e) {
		if (this.vista.btnAviones==e.getSource()){
			
		} 
		if(this.vista.btnClientes==e.getSource()){
			
		}
		if(this.vista.btnProveedores==e.getSource()){
			
		}
		if(this.vista.btnPruebas==e.getSource()){
			
		}
		if(this.vista.btnPedidos==e.getSource()){
			
		}
		if(this.vista.btnMateriales==e.getSource()){
			
		}
		if(this.vista.btnEnvios==e.getSource()){
			
		}
		if (this.vista.btnCerrarSesion==e.getSource()){
			
		}
	}

}
