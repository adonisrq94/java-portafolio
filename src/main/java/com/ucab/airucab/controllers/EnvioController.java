package com.ucab.airucab.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import com.ucab.airucab.models.Envio;
import com.ucab.airucab.models.Validaciones;
import com.ucab.airucab.views.*;



public class EnvioController implements ActionListener {
	public EnvioCreateView vistaCrear;
	public Envio envio;
	public Validaciones validar;
	
	public EnvioController(){
	
		this.envio = new Envio();
		this.validar = new Validaciones();
	}
	
	private void initializerView( ) {

		this.vistaCrear = new EnvioCreateView();		
		this.vistaCrear.btnRegistrar.addActionListener(this);
		this.vistaCrear.btnRegresar.addActionListener(this);
		this.vistaCrear.FrInicio.setVisible(true);
	}
	

	
	public boolean validarEnvio(EnvioCreateView vistaCrear){
		boolean flag=true ;
		if(!validar.validarSoloLetras(vistaCrear.txtStatusEnvio.getText())) {
			vistaCrear.txtStatusEnvio.setText("debe ingresar solo letras");
			flag = false;
			
		}
		else {
			envio.setStatusEnvio(vistaCrear.txtStatusEnvio.getText());
			System.out.print(envio.getStatusEnvio());
		}
		if (vistaCrear.calendar.getDate() == null){
			System.out.println("ingrese una fecha");
			flag = false;
		}else
			envio.getFecha().setFechaDate(new Date(vistaCrear.calendar.getDate().getTime()));
	
	if (vistaCrear.calendarF.getDate() == null){
		System.out.println("ingrese una fecha");
		flag = false;
	}else
		envio.getFecha().setFechaDate(new Date(vistaCrear.calendarF.getDate().getTime()));
    	
		return flag;
		
	}
	

	@Override
	/*public void actionPerformed(ActionEvent e) {
		
		
		if (this.vistaCrear.btnRegistrar== e.getSource()){
			if(validarEnvio(vistaCrear)){
				
				if (envio.crearEnvio()){
			  		System.out.println("hola");			  		
			  		vistaCrear.FrInicio.setVisible(false);
                    show.table.setModel(
            		cliente.tableShow(show.col,show.btn_modificar,show.btn_eliminar,show.btn_ver));

				}
				else{
			  		System.out.println("adios");
				}
			}
			
		}
		if(this.vistaCrear.btnRegresar==e.getSource()){
				
		}

		
	}*/
	
    public Envio getEnvio() {
		return envio;
	}

	public void setEnvio(Envio envio) {
		this.envio = envio;
	}
	
/*	private void tableMouseClicked(MouseEvent e) {//GEN-FIRST:event_tablaMouseClicked
        
        int column = show.table.getColumnModel().getColumnIndexAtX(e.getX());
        int row = e.getY()/show.table.getRowHeight();
        Envio envio = new Envio();
        
        if(row < show.table.getRowCount() && row >= 0 && column < show.table.getColumnCount() && column >= 0){
            Object value = show.table.getValueAt(row, column);
            if(value instanceof JButton){
                ((JButton)value).doClick();
                JButton boton = (JButton) value;

                if(boton.getName().equals("m")){
                    System.out.println("Click en el boton modificar");
                    System.out.println(show.table.getValueAt(row, 0) );
                    //EVENTOS MODIFICAR
                }
                if(boton.getName().equals("e")){
                    JOptionPane.showConfirmDialog(null, "Desea eliminar este Cliente?", "Confirmar", JOptionPane.OK_CANCEL_OPTION);
                    System.out.println("Click en el boton eliminar");
                    envio.setRif(show.table.getValueAt(row, 0).toString());
                    envio.eliminarEnvio();
                    show.table.setModel(
            		envio.tableShow(show.col,show.btn_modificar,show.btn_eliminar,show.btn_ver));
                    //EVENTOS ELIMINAR
                }
                if(boton.getName().equals("v")){
                    System.out.println("Click en el boton ver");
                    //EVENTOS MODIFICAR
                }
            }

        }

}*/
}