package com.ucab.airucab.controllers;


import com.ucab.airucab.models.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.ucab.airucab.views.*; 

public class LoginController implements ActionListener {
	
	public LoginView vista;
	public Login login;
	private Validaciones validar;
	
	public LoginController() {
		this.vista = new LoginView();
		this.login = new Login();
		this.validar = new Validaciones();
		this.initializerView();
		
	}
	
	
	public void initializerView() {
		
		this.vista.btn_login.addActionListener(this);
		this.vista.FrInicio.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e) {
		if (this.vista.btn_login == e.getSource())	{	
		  	
			if (validarLogin()) {		  		 
			  	
		  		if(login.login()){
			  		//TODO poner controlador
			  		System.out.println("hola");
			  	}else {
			  	   //TODO poner label con error 
			  		System.out.println("adios");
			  	}
		  	}
		}
		
	}
	
	public boolean validarLogin() {
		boolean flag = true;
		
		if(!validar.validarSoloLetras(vista.text_id.getText())) {
			vista.text_id.setText("debe ingresar solo letras");
			flag = false;
		}else {
			this.login.setUsername(vista.text_id.getText());
		}
		if(!validar.validarSoloLetrasNumeros(vista.text_pass.getText())) {
			vista.text_pass.setText("debe ingresar solo letras o numeros");
			flag = false;
		}else {
			this.login.setPassword(vista.text_pass.getText());;
		}
		return flag;
	}
	
	

}
