package com.ucab.airucab.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;


import com.ucab.airucab.models.Proveedor;
import com.ucab.airucab.models.Validaciones;
import com.ucab.airucab.views.ProveedorCreateView;


public class ProveedorController implements ActionListener {
	public ProveedorCreateView vistaCrear;
	public Validaciones validar;
	public Proveedor proveedor;
	
	public ProveedorController (){
		this.proveedor = new Proveedor();
		this.validar = new Validaciones();
		this.initializerView();
	}
	
	private void initializerView( ) {
		
		this.vistaCrear = new ProveedorCreateView();			
		
		this.vistaCrear.btnRegistrar.addActionListener(this);
		this.vistaCrear.btnRegresar.addActionListener(this);
		this.vistaCrear.btnAgregarCorreos.addActionListener(this);
		this.vistaCrear.btnAgregarRedes.addActionListener(this);
		this.vistaCrear.btnAgregarTelefonos.addActionListener(this);
		this.vistaCrear.FrInicio.setVisible(true);
	}
	

	
	public boolean validarProveedor ( ProveedorCreateView vistaCrear){
		boolean flag=true ;
		if(!validar.validarSoloLetras(vistaCrear.txtNombre.getText())) {
			vistaCrear.txtNombre.setText("debe ingresar solo letras");
			flag = false;
			
		}
		else {
			proveedor.setNombre(vistaCrear.txtNombre.getText());
			System.out.print(proveedor.getNombre());
		}
		if (!validar.validarVacio(vistaCrear.txtRif.getText())){
			vistaCrear.txtRif.setText("Este campo no puede quedar vacio");
			flag =false;
		}
		else{
			proveedor.setRif(vistaCrear.txtRif.getText());
			System.out.println(proveedor.getRif());
		}
		if (vistaCrear.calendar.getDate() == null){
			System.out.println("ingrese una fecha");
			flag = false;
		}else
			proveedor.getFecha().setFechaDate(new Date(vistaCrear.calendar.getDate().getTime()));
				
		
		return flag;
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (this.vistaCrear.btnRegistrar== e.getSource()){
			if(validarProveedor(vistaCrear)){
				
				if (proveedor.crearProveedor()){
			  		System.out.println("hola");

				}
				else{
			  		System.out.println("adios");
				}
			}
			
		}
		if(this.vistaCrear.btnRegresar==e.getSource()){
			
			
		}
		if(this.vistaCrear.btnAgregarTelefonos==e.getSource()){
			
		}
		
	}

}
