package com.ucab.airucab.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.*;

public class AvionDao extends ConnectionMysql implements Dao<Avion> {

	public AvionDao() {
		 
	}
	  
		
	    @Override
		public void create(Avion avion) {
			int clave =0; 
			String	query = "INSERT into AVION(avi_nombre,avi_capacidad,avi_longitud,avi_altura,avi_tipo_nave,avi_modelo)"+
			"VALUES ('"+avion.getNombre() +"','"+avion.getCapacidad()+"','"+avion.getLongitud() +"','"+avion.getAltura()+"','"+avion.getTipo()+"','"+avion.getModelo()+"')";
			 
			avion.setStatus(openClonnectionUpdate(query));
			System.out.println(query);
			try { 
				ResultSet key = this.statement.getGeneratedKeys();
				while(key.next()){
					clave=key.getInt(1);
					System.out.println("clave"+clave);
					}
				if(clave>0) {
			    	for (Pieza pieza : avion.getPiezas() ) {
			    		query = "INSERT into PIEZA_AVION(fk_avi_id,fk_pie_cod)"+
			    				"VALUES ('"+clave+"','"+pieza.getId()+"')";
			    		avion.setStatus(openClonnectionUpdate(query));
				}
			    	}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				closeClonnection();
				}
			
		}

		@Override
		public void read(Avion dao) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void update(Avion dao) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void delete(Avion dao) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void report(Avion dao) {
			// TODO Auto-generated method stub
			
		}
		
		
		public ArrayList<Pieza> readAll() {
			ResultSet rs = openClonnectionExecute("Select* FROM PIEZA");
			ArrayList<Pieza> piezas = new ArrayList<Pieza>();
			
			try{
				while (rs.next()) {
					Pieza pieza = new Pieza();
					pieza.setId(rs.getInt("pie_cod"));
					pieza.setNombre(rs.getString("pie_nombre"));
					pieza.setDescripcion(rs.getString("pie_descripcion"));
					pieza.setDuraccion(rs.getInt("pie_duracion_construccion"));
					System.out.println(rs.getString("pie_nombre"));
					piezas.add(pieza);

					
				}
			}
			catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally {
			  closeClonnection();
			}
				
		  return piezas;
		}

}
