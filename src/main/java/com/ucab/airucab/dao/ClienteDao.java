package com.ucab.airucab.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.ConnectionMysql;
import com.ucab.airucab.models.Cliente;

public class ClienteDao  extends ConnectionMysql implements Dao<Cliente> {


	public ClienteDao( ){
         super();
	}
	

	@Override
	public void create(Cliente cliente) {
		 String d = "11/12/12";
		 String	query = "INSERT into CLIENTE(cli_rif,cli_Nombre,cli_fecha_inicio_operaciones,fk_lug_cod)"+
		"VALUES ('"+cliente.getRif()+"','"+cliente.getNombre()+"','"+d+"','"+1+"')";
		 cliente.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
	}

	@Override
	public void read (Cliente cliente) {
		ResultSet rs = openClonnectionExecute("Select* FROM CLIENTE WHERE pro_rif= '"+cliente.getRif()+"' ");
		try{
			while (rs.next()) {
				cliente.setRif(rs.getString("pro_rif"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
	}

	@Override
	public void update(Cliente cliente) {
		 String	query = "UPDATE into PROVEEDOR(pro_rif,pro_Nombre,pro_fecha_inicio_operaciones,fk_lug_cod)"+
		"VALUES ('"+cliente.getRif()+"','"+cliente.getNombre()+"','"+cliente.getFecha()+"','"+cliente.getDireccion()+"')";
		cliente.setStatus(openClonnectionUpdate(query));
		
		
	}

	@Override
	public void delete(Cliente cliente) {
		 cliente.setStatus(openClonnectionUpdate("DELETE FROM CLIENTE WHERE cli_rif= '"+cliente.getRif()+"' "));
		 closeClonnection();
	}		
	

	@Override
	public void report(Cliente cliente) {
		// TODO Auto-generated method stub
		
	}
	
	public ArrayList<Cliente> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM CLIENTE");
		ArrayList<Cliente> clientes = new ArrayList<Cliente>();
		
		try{
			while (rs.next()) {
				Cliente cliente = new Cliente();
				cliente.setRif(rs.getString("cli_rif"));
				clientes.add(cliente);
				System.out.println(rs.getString("cli_rif"));
				
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return clientes;
	}
	




}
