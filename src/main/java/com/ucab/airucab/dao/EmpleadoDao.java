package com.ucab.airucab.dao;


import java.sql.ResultSet;
import java.sql.SQLException;

import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.ConnectionMysql;
import com.ucab.airucab.models.Empleado;

public class EmpleadoDao extends ConnectionMysql implements Dao<Empleado>  {
	
	
	
	
	public EmpleadoDao( ){
         super();
	}
	

	@Override
	public void create(Empleado empleado) {
		 String	query = "INSERT into EMPLEADO(emp_rif,emp_nombre,emp_fecha_nacimiento,emp_apellido,emp_titulo,emp_cargo_empleado,emp_años_servicio,fk_lug_cod)"+
		"VALUES ('"+empleado.getRif()+"','"+empleado.getNombre()+"','"+empleado.getFecha().getFechaString()+"','"+empleado.getApellido()+"','"+empleado.getTitulo()+"','"+empleado.getCargoempleado()+"','"+empleado.getTiemservicio()+"','"+1+"')";
		 empleado.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
	}

	@Override
	public void read(Empleado empleado) {
		ResultSet rs = openClonnectionExecute("Select* FROM EMPLEADO WHERE emp_rif= '"+empleado.getRif()+"' ");
		try{
			while (rs.next()) {
				empleado.setRif(rs.getString("emp_rif"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
	}

	@Override
	public void update(Empleado empleado) {
		 String	query = "UPDATE into EMPLEADO(emp_rif,emp_Nombre,emp_fecha_nacimiento,emp_apellido, emp_titulo, emp_cargo_empleado,emp_años_servicio,fk_lug_cod)"+
		"VALUES ('"+empleado.getRif()+"','"+empleado.getNombre()+"','"+empleado.getFecha()+"','"+empleado.getDireccion()+"')";
		empleado.setStatus(openClonnectionUpdate(query));
		
	}

	@Override
	public void delete(Empleado empleado) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void report(Empleado empleado) {
		// TODO Auto-generated method stub
		
	}


}
