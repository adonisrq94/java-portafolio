	package com.ucab.airucab.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.Cliente;
import com.ucab.airucab.models.ConnectionMysql;
import com.ucab.airucab.models.Material;

public class MaterialDao extends ConnectionMysql implements Dao<Material> {

	public MaterialDao()  {
		super();
	}

	@Override
	public void create(Material material) {
		 String	query = "INSERT into MATERIAL(mat_nombre)"+
					"VALUES ('"+material.getNombre()+"')";
		System.out.println(query);			 
		 material.setStatus(openClonnectionUpdate(query));
					 closeClonnection();
		
	}

	@Override
	public void read(Material material) {
		ResultSet rs = openClonnectionExecute("Select* FROM MATERIAL WHERE mat_cod= '"+material.getId()+"' ");
		try{
			while (rs.next()) {
				material.setId(rs.getInt("mat_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
		
	}

	public void readAll(Material material) {
		ResultSet rs = openClonnectionExecute("Select* FROM MATERIAL ");
		try{
			while (rs.next()) {
				material.setId(rs.getInt("mat_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
		
	}
	
	@Override
	public void update(Material material) {
	  String	query = "UPDATE into MATERIAL(mat_Nombre)"+
	  "VALUES ('"+material.getNombre()+"')";
	  material.setStatus(openClonnectionUpdate(query));
				
	} 

	@Override
	public void delete(Material material) {
		 String	query = "DELETE FROM MATERIAL WHERE mat_cod = "+material.getId();			 
		 material.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
		
	}

	@Override
	public void report(Material dao) {
		// TODO Auto-generated method stub
		
	}
	
	public ArrayList<Material> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM CLIENTE");
		ArrayList<Material> materiales = new ArrayList<Material>();
		
		try{
			while (rs.next()) {
				Material material = new Material();
				material.setId(rs.getInt("mat_cod")); 
				materiales.add(material);
				System.out.println(rs.getString("mat_cod"));
				
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return materiales;
	}
	


}
