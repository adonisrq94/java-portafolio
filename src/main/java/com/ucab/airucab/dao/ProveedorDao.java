package com.ucab.airucab.dao;


import java.sql.ResultSet;
import java.sql.SQLException;

import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.ConnectionMysql;
import com.ucab.airucab.models.Proveedor;

public class ProveedorDao extends ConnectionMysql implements Dao<Proveedor>  {
	
	
	
	
	public ProveedorDao( ){
         super();
	}
	

	@Override
	public void create(Proveedor proveedor) {
		 String	query = "INSERT into PROVEEDOR(pro_rif,pro_Nombre,pro_fecha_inicio_operaciones,fk_lug_cod)"+
		"VALUES ('"+proveedor.getRif()+"','"+proveedor.getNombre()+"','"+proveedor.getFecha().getFechaString()+"','"+1+"')";
		 proveedor.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
	}

	@Override
	public void read(Proveedor proveedor) {
		ResultSet rs = openClonnectionExecute("Select* FROM PROVEEDOR WHERE pro_rif= '"+proveedor.getRif()+"' ");
		try{
			while (rs.next()) {
				proveedor.setRif(rs.getString("pro_rif"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
	}

	@Override
	public void update(Proveedor proveedor) {
		 String	query = "UPDATE into PROVEEDOR(pro_rif,pro_Nombre,pro_fecha_inicio_operaciones,fk_lug_cod)"+
		"VALUES ('"+proveedor.getRif()+"','"+proveedor.getNombre()+"','"+proveedor.getFecha()+"','"+proveedor.getDireccion()+"')";
		proveedor.setStatus(openClonnectionUpdate(query));
		
		
	}

	@Override
	public void delete(Proveedor proveedor) {
		ResultSet rs = openClonnectionExecute("DELETE FROM PROVEEDOR WHERE pro_rif= '"+proveedor.getRif()+"' ");
		try{
			while (rs.next()) {
				proveedor.setRif(rs.getString("pro_rif"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
	}

	@Override
	public void report(Proveedor proveedor) {
		// TODO Auto-generated method stub
		
	}


}
