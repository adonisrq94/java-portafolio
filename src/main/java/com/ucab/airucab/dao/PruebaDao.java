package com.ucab.airucab.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.ucab.airucab.interfaces.Dao;
import com.ucab.airucab.models.ConnectionMysql;
import com.ucab.airucab.models.Material;
import com.ucab.airucab.models.Prueba;

public class PruebaDao extends ConnectionMysql implements Dao<Prueba>{

	public PruebaDao() {
		super();
	}
	@Override
	public void create(Prueba prueba) {
		 String	query = "INSERT into PRUEBA(pru_nombre)"+
					"VALUES ('"+prueba.getNombre()+"')";
		System.out.println(query);			 
		 prueba.setStatus(openClonnectionUpdate(query));
					 closeClonnection();
		
	}

	@Override
	public void read(Prueba prueba) {
		ResultSet rs = openClonnectionExecute("Select* FROM PRUEBA WHERE pru_cod= '"+prueba.getId()+"' ");
		try{
			while (rs.next()) {
				prueba.setId(rs.getInt("pru_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
		
	}

	public void readAll(Prueba prueba) {
		ResultSet rs = openClonnectionExecute("Select* FROM PRUEBA ");
		try{
			while (rs.next()) {
				prueba.setId(rs.getInt("pru_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
		
	}
	
	@Override
	public void update(Prueba prueba) {
	  String	query = "UPDATE into PRUEBA(mat_Nombre)"+
	  "VALUES ('"+prueba.getNombre()+"')";
	  prueba.setStatus(openClonnectionUpdate(query));
				
	} 

	@Override
	public void delete(Prueba prueba) {
		 String	query = "DELETE FROM PRUEBA WHERE mat_cod = "+prueba.getId();			 
		 prueba.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
		
	}

	@Override
	public void report(Prueba prueba) {
		// TODO Auto-generated method stub
		
	}

}
