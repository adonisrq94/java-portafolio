package com.ucab.airucab.models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.ucab.airucab.dao.ClienteDao;

public class Cliente  extends Empresa {


	private ClienteDao dao;


	public Cliente(){
		this.dao = new ClienteDao();
	}

	public boolean crearCliente(){
		fecha.convertirDateString(fecha.getFechaDate());;
		dao.create(this);
		if (status>0)
			return true;
		else
			return false;

	}

	public void modificarCliente(){
		dao.update(this);

	}

	public void buscarCliente(){


	}


	public ArrayList<Cliente> mostrarCliente(){
		ArrayList<Cliente> clientes = dao.readAll();
		return clientes;
	}


	public boolean eliminarCliente(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}

    public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar, JButton ver){
    	ArrayList<Cliente>  clientes = mostrarCliente();
    	col = new Object[]{"Codigo","Nombre","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	
    	for (Cliente cliente : clientes) {
    		tableModel.addRow(new Object[]{cliente.getRif(),cliente.getNombre(),modificar,eliminar,ver});
    	}
    	return tableModel;
    }
}
