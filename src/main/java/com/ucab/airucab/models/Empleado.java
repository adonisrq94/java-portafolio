package com.ucab.airucab.models;

import com.ucab.airucab.dao.EmpleadoDao;

public class Empleado {
	
	public String rif ;
	public String nombre;
	public String apellido;
	public Fecha fecha;
	public int direccion;
	public String titulo;
	public int tiemservicio;
	public String cargoempleado;
	
	
	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public int getTiemservicio() {
		return tiemservicio;
	}


	public void setTiemservicio(int tiemservicio) {
		this.tiemservicio = tiemservicio;
	}


	public String getCargoempleado() {
		return cargoempleado;
	}


	public void setCargoempleado(String cargoempleado) {
		this.cargoempleado = cargoempleado;
	}

	public int status;


	private EmpleadoDao dao;


	public Empleado(){
		this.status = -1;
		this.fecha = new Fecha();
	}


	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getRif() {
		return rif;
	}

	public void setRif(String rif) {
		this.rif = rif;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getDireccion() {
		return direccion;
	}

	public void setDireccion(int direccion) {
		this.direccion = direccion;
	}

	public Fecha getFecha() {
		return fecha;
	}

	public void setFecha(Fecha fecha) {
		this.fecha = fecha;
	}


}