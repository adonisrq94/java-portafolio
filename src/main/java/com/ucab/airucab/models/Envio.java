package com.ucab.airucab.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import com.ucab.airucab.dao.EnvioDao;

public class Envio  {
		
		private String StatusEnvio ;
		private Fecha fecha;
		private int codigo;
		public int getCodigo() {
			return codigo;
		}

		public void setCodigo(int codigo) {
			this.codigo = codigo;
		}


		private int MontoAcreditado;
		private Envio envio; 
		
		 public Envio getEnvio() {
				return envio;
			}

			public void setEnvio(Envio envio) {
				this.envio = envio;
			}
			



		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}
		
		public String getStatusEnvio() {
			return StatusEnvio;
		}

		public void setStatusEnvio(String StatusEnvio) {
			this.StatusEnvio = StatusEnvio;
		}
		
		public Fecha getFecha() {
			return fecha;
		}

		public void setFecha(Fecha fecha) {
			this.fecha = fecha;
		}


	private EnvioDao dao;


	public Envio(){
		this.dao = new EnvioDao();
	}

	public boolean crearEnvio(){
		fecha.convertirDateString(fecha.getFechaDate());;
		dao.create(this);
		if (status>0)
			return true;
		else
			return false;

	}

	public void modificarEnvio(){
		dao.update(this);

	}

	public void buscarEnvio(){


	}


/*	public ArrayList<Envio> mostrarEnvio(){
		ArrayList<Envio> envio = dao.readAll();
		return envios;
	}

*/
	public boolean eliminarEnvio(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}

}
