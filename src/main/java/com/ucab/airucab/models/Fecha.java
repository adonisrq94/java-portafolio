package com.ucab.airucab.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Fecha {

    private Date fechaDate;
    private String fechaString;
	
    public Fecha() {
		// TODO Auto-generated constructor stub
	}
    
    public void convertirDateString(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int d=calendar.get(Calendar.DAY_OF_MONTH);
		int m =calendar.get(Calendar.MONTH)+1;
		int y= calendar.get(Calendar.YEAR);
		fechaString=y+"-"+m+"-"+d;
    }
    
    public void convertirStringDate(String fecha) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-dd-M");
		try {
			fechaDate = format.parse(fecha);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	public Date getFechaDate() {
		return fechaDate;
	}

	public void setFechaDate(Date fechaDate) {
		this.fechaDate = fechaDate;
	}

	public String getFechaString() {
		return fechaString;
	}

	public void setFechaString(String fechaString) {
		this.fechaString = fechaString;
	}

	public void setDate(Date date) {
		// TODO Auto-generated method stub
		
	}



}
