package com.ucab.airucab.models;


import com.ucab.airucab.dao.*;


public class Login {
    
	private Validaciones validar;
	private LoginDao dao;
	private String username;
	private String password;
	private int id;

	
	public Login() {
       this.dao = new LoginDao();
	}
	
	public boolean login() {
	   dao.read(this);
	   if (id>0)
		   return true;
	   else 
		   return false;
		   
	}
	
	public Validaciones getValidar() {
		return validar;
	}

	public void setValidar(Validaciones validar) {
		this.validar = validar;
	}

	public LoginDao getDao() {
		return dao;
	}

	public void setDao(LoginDao dao) {
		this.dao = dao;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void logout() {
		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

}
