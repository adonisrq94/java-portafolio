package com.ucab.airucab.models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.ucab.airucab.dao.MaterialDao;

public class Material {
	
	private int id;
	private int status; 
	private String nombre;
    private MaterialDao dao;
    private int cantidad;

	public Material() {
      this.dao = new MaterialDao();	   	
	}
	
	public Material(int id , String nombre,int cantidad) {
	    this.id = id;
	    this.nombre = nombre;
	    this.cantidad = cantidad;
		this.dao = new MaterialDao();	   	
		}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public boolean crearMaterial() {
		
		dao.create(this);
		if (status>0)
			return true;
		else 
			return false;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


}
