package com.ucab.airucab.models;

import com.ucab.airucab.dao.ProveedorDao;

public class Proveedor extends Empresa {
	

	private ProveedorDao dao;


	public Proveedor(){
        super();
		this.dao = new ProveedorDao();
	}

	public boolean crearProveedor(){
		
		fecha.convertirDateString(fecha.getFechaDate());; 
		dao.create(this);
		if (status>0)
			return true;
		else 
			return false;
		
	}
	
	public void modificarProveedor(){
		dao.update(this);		
		
	}
	
	public void buscarProveedor(){
		
		
	}
	
	public boolean eliminarProveedor(){
		dao.delete(this);
		if (status>0)
			return true;
		else 
			return false;
		
	}

	

}
 