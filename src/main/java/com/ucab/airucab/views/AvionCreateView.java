package com.ucab.airucab.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDateChooser;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class AvionCreateView extends JFrame {
	
	public JScrollPane scrollPane;
	public JTable table_pieza;
	public JPanel contentPane;
	public JTextField txtModelo;
	public JTextField txtNombre; 
	public JTextField txtCapacidad;
	public JTextField txtLongitud;
	public JTextField txtAltura;
	public JTextField txtTipo;
	public JFrame FrInicio;
	public JButton btnCrear;
	public JButton btnRegresar;
	public DefaultTableModel tableModel;
	public Object[] col;

	public  AvionCreateView()   {
		initializerView();
		ver_tabla();
		
	}
	public void initializerView(){
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Nuevo Avion");
		FrInicio.setBounds(100, 100, 600, 406);
		FrInicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(-18, 64, 512, 313);
		contentPane.setLayout(null);
		
		txtNombre = new JTextField();
		txtNombre.setText("Nombre");
		txtNombre.setBounds(31, 224, 114, 19);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblCapacidad = new JLabel("Capacidad del avion");
		lblCapacidad.setBounds(28, 63, 117, 15);
		contentPane.add(lblCapacidad);
		
		txtCapacidad = new JTextField();
		txtCapacidad.setText("Cantidad de pasajeros");
		txtCapacidad.setBounds(31, 89, 114, 19);
		contentPane.add(txtCapacidad);
		txtCapacidad.setColumns(10);
		
		JLabel lblAltura = new JLabel("Altura que alcanza");
		lblAltura.setBounds(31, 119, 101, 15);
		contentPane.add(lblAltura);
		
		txtAltura = new JTextField();
		txtAltura.setText("Altura");
		txtAltura.setBounds(31, 145, 142, 19);
		contentPane.add(txtAltura);
		txtAltura.setColumns(10);
		
		JLabel lblLongitud = new JLabel("Longitud que posee");
		lblLongitud.setBounds(31, 11, 101, 15);
		contentPane.add(lblLongitud);
		
		txtLongitud = new JTextField();
		txtLongitud.setText("Longitud");
		txtLongitud.setBounds(28, 37, 117, 21);
		contentPane.add(txtLongitud);
			
		JLabel lblTipo = new JLabel("Tipo de avion");
		lblTipo.setBounds(22, 241, 114, 19);
		contentPane.add(lblCapacidad);
		
		txtTipo = new JTextField();
		txtTipo.setText("Tipo de aeronave");
		txtTipo.setBounds(31, 193, 114, 19);
		contentPane.add(txtTipo);
		txtTipo.setColumns(10);
		
		btnCrear = new JButton("Crear Avion");
		btnCrear.setBounds(237, 41, 192, 25);
		contentPane.add(btnCrear);
		
		scrollPane = new JScrollPane();
        table_pieza = new JTable();
		table_pieza.setModel(new javax.swing.table.DefaultTableModel(
	            new Object [][] {
	                {null, null, null, null, null}
	            },
	            new String [] {
	                "Title 1", "Title 2", "Title 3", "Title 4","Title 5"}
	        ));
		
	    table_pieza.setBounds(28, 51, 382, 231);
	    
	    scrollPane = new JScrollPane(table_pieza);
	    scrollPane.setBounds(207, 130, 236, 157);
		contentPane.add(scrollPane,BorderLayout.CENTER);
		

	    
		btnRegresar = new JButton("Regresar");
	    btnRegresar.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    	}
	    });
	    btnRegresar.setBounds(236, 317, 120, 25);
	    contentPane.add(btnRegresar);
		
		FrInicio.getContentPane().add(contentPane);
		
		JLabel lblModelo = new JLabel("Modelo");
		lblModelo.setBounds(10, 19, 101, 15);
		FrInicio.getContentPane().add(lblModelo);
		
		txtModelo = new JTextField();
		txtModelo.setBounds(10, 34, 114, 19);
		FrInicio.getContentPane().add(txtModelo);
		txtModelo.setText("Modelo del avion");
		txtModelo.setColumns(10);
		FrInicio.setResizable(true);
		
	}
	
	 public void ver_tabla(){ 
	        table_pieza.setDefaultRenderer(Object.class, new Render());
	        col = new Object[]{"id","Nombre","modelo","duracion","select"};
	        tableModel = new DefaultTableModel(new Object [][] {},col)
	        {
	            public boolean isCellEditable(int row, int column){
	                return false;
	            }

	        		public Class<?> getColumnClass(int column){
	        			switch (column) {
	        			case 0:
	        				return String.class;
	        			case 1:
	        				return String.class;
	        			case 2:
	        				return String.class;
	        			case 3:
	        				return String.class;
	        			case 4:
	        				return Boolean.class;
	        			default:
	            			return String.class;
	            			
	        			}
	        			
	        	};
	        };
	        table_pieza.setModel(tableModel);
	        table_pieza.setPreferredScrollableViewportSize(table_pieza.getPreferredSize());
	        
	   
}
}
