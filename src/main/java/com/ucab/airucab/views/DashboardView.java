package com.ucab.airucab.views;



import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class DashboardView extends JFrame {

	private JPanel contentPane;
	public JFrame FrInicio;
	public JPanel panel;
	public JButton btnAviones;
	public JButton btnPedidos;
	public JButton btnEnvios;
	public JButton btnProveedores;
	public JButton btnPruebas;
	public JButton btnClientes;
	public JButton btnMateriales;
	public JButton btnCerrarSesion;
	
	public DashboardView() {
		initializerView();

	}
	public void initializerView(){
		FrInicio = new JFrame();
		FrInicio.setTitle("AirUcab");
		//FrInicio.setBounds(100, 100, 600, 406);
		//FrInicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//FrInicio.getContentPane().setLayout(null);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 642, 412);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel = new JPanel();
		panel.setBounds(425, 69, 191, 281);
		contentPane.add(panel);
		panel.setLayout(null);
		
		btnAviones = new JButton("Aviones");
		btnAviones.setBounds(12, 5, 167, 25);
		panel.add(btnAviones);
		
		btnPedidos = new JButton("Pedidos");
		btnPedidos.setBounds(12, 42, 167, 25);
		panel.add(btnPedidos);
		
		btnEnvios = new JButton("Envios");
		btnEnvios.setBounds(12, 79, 167, 25);
		panel.add(btnEnvios);
		
		btnProveedores = new JButton("Proveedores");
		btnProveedores.setBounds(12, 116, 167, 25);
		panel.add(btnProveedores);
		
		btnPruebas = new JButton("Pruebas");
		btnPruebas.setBounds(12, 153, 167, 25);
		panel.add(btnPruebas);
		
		btnClientes = new JButton("Clientes");
		btnClientes.setBounds(12, 189, 167, 25);
		panel.add(btnClientes);
		
		btnMateriales = new JButton("Materiales");
		btnMateriales.setBounds(12, 222, 167, 25);
		panel.add(btnMateriales);
		
	
		
		btnCerrarSesion = new JButton("Cerrar sesion");
		btnCerrarSesion.setBounds(476, 12, 140, 25);
		contentPane.add(btnCerrarSesion);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(12, 69, 401, 281);
		contentPane.add(panel_1);
		
	}
}
