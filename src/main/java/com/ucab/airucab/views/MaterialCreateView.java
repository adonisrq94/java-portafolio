package com.ucab.airucab.views;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;


@SuppressWarnings("serial")
public class MaterialCreateView extends JFrame {

	public JPanel contentPane;
	public JFrame FrInicio;
	public JTextField nombre_text;
	public JButton agregar_btn;
	/**
	 * Launch the application.
	 */
	/**
	 * Create the frame.
	 */
	public MaterialCreateView() {
		InicialitationView();
	}
	public void InicialitationView() {
		FrInicio = new JFrame();
		FrInicio.setTitle("Nuevo Material");
		FrInicio.setBounds(0, 0, 500, 500);
		FrInicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(0, 0, 500, 500);
		contentPane.setLayout(null);
		
			
			
        nombre_text = new JTextField();
        nombre_text.setBounds(23, 98, 253, 20);
        nombre_text.setColumns(20);
        contentPane.add(nombre_text);
        
        
        agregar_btn = new JButton("Agregar");
        agregar_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	}
        });
		agregar_btn.setBounds(302, 98, 114, 25);
		contentPane.add(agregar_btn);
        
    
        
        JLabel lblNombreDeMaterial = new JLabel("Nombre de Material");
        lblNombreDeMaterial.setBounds(23, 73, 152, 14);
        contentPane.add(lblNombreDeMaterial);
        
		FrInicio.getContentPane().add(contentPane);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FrInicio.setResizable(false);
	}
}
