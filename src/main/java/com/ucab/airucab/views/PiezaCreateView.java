package com.ucab.airucab.views;



import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.BorderLayout;
import java.awt.List;
import javax.swing.JTable;
import java.awt.Component;
import java.awt.Dimension;

@SuppressWarnings("serial")
public class PiezaCreateView extends JFrame {



	public JPanel contentPane;
	public JTextField text_nombre;
	public JTextField text_modelo;
	public JTextField text_tiempo;
	public JButton btn_agregar_material;
	public JButton btn_crear;
	public JFrame  FrInicio;
	public JTable table;
    public JScrollPane jScrollPane;
    public DefaultTableModel tableModel;
    public DefaultTableModel tableModelPrueba;
    public Object[] col;
    public JCheckBox check_material ;
    public JTextField text_descripcion;
    public JScrollPane scrollPane;
    public JTable table_pruebas;


	public PiezaCreateView() {
		initialization();
		ver_tabla();
	}
	
	private void initialization() {
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Crear pieza");
		FrInicio.setBounds(0, 0, 800, 328);
		FrInicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(0, 0, 800, 328);
		contentPane.setLayout(null);
		
		scrollPane = new JScrollPane((Component) null);
		scrollPane.setBounds(467, 62, 236, 157);
		contentPane.add(scrollPane);
		
		table_pruebas = new JTable();
		table_pruebas.setPreferredScrollableViewportSize(new Dimension(300, 0));
		scrollPane.setViewportView(table_pruebas);
		
		
		text_nombre = new JTextField();
		text_nombre.setBounds(40, 100, 114, 19);
		contentPane.add(text_nombre);
		text_nombre.setColumns(10);
		
		text_modelo = new JTextField();
		text_modelo.setBounds(40, 120, 114, 19);
		contentPane.add(text_modelo);
		text_modelo.setColumns(10);
		
		text_tiempo = new JTextField();
		text_tiempo.setBounds(40, 140, 114, 19);
		contentPane.add(text_tiempo);
		text_tiempo.setColumns(10);
		
		text_descripcion = new JTextField();
		text_descripcion.setBounds(40, 163, 114, 19);
		contentPane.add(text_descripcion);
		text_descripcion.setColumns(10);
		
		btn_crear = new JButton("Crear");
		btn_crear.setBounds(40, 231, 114, 25);
		contentPane.add(btn_crear);
		
		btn_agregar_material = new JButton("Agregar");
		btn_agregar_material.setBounds(181, 231, 236, 25);
		contentPane.add(btn_agregar_material);
		
	    jScrollPane = new JScrollPane();
        table = new JTable();
		table.setModel(new javax.swing.table.DefaultTableModel(
	            new Object [][] {
	                {null, null, null, null}
	            },
	            new String [] {
	                "Title 1", "Title 2", "Title 3", "Title 4"}
	        ));
		
	    table.setBounds(28, 51, 382, 231);
	    
	    jScrollPane = new JScrollPane(table);
	    jScrollPane.setBounds(181, 62, 236, 157);
		contentPane.add(jScrollPane,BorderLayout.CENTER);
		
		FrInicio.getContentPane().add(contentPane);
		

		
		FrInicio.setResizable(false);
	}
	
	   public void ver_tabla(){
	        check_material = new JCheckBox("c"); 
	        check_material.setName("c");
	        table.setDefaultRenderer(Object.class, new Render());
	        col = new Object[]{"id","Nombre","select","cantidad"};
	        tableModel = new DefaultTableModel(new Object [][] {},col)
	        {
	            public boolean isCellEditable(int row, int column){
	                return false;
	            }

	        		public Class<?> getColumnClass(int column){
	        			switch (column) {
	        			case 0:
	        				return String.class;
	        			case 1:
	        				return String.class;
	        			case 2:
	        				return Boolean.class;
	        			case 3:
	        				return String.class;
	        			default:
	            			return String.class;	
	        			}
	        			
	        	};
	        };
	        table.setModel(tableModel);
	        table.setPreferredScrollableViewportSize(table.getPreferredSize());
	        

	        table_pruebas.setDefaultRenderer(Object.class, new Render());
	        col = new Object[]{"id","Nombre","duracion","select"};
	        tableModelPrueba = new DefaultTableModel(new Object [][] {},col)
	        {
	            public boolean isCellEditable(int row, int column){
	                return false;
	            }

	        		public Class<?> getColumnClass(int column){
	        			switch (column) {
	        			case 0:
	        				return String.class;
	        			case 1:
	        				return String.class;
	        			case 2:
	        				return String.class;
	        			case 3:
	        				return Boolean.class;
	        			default:
	            			return String.class;	
	        			}
	        			
	        	};
	        };
	        table_pruebas.setModel(tableModelPrueba);
	        table_pruebas.setPreferredScrollableViewportSize(table_pruebas.getPreferredSize());
	  

	    }
}
