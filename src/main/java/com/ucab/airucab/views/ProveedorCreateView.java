package com.ucab.airucab.views;



import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;

@SuppressWarnings("serial")
public class ProveedorCreateView extends JFrame {

	public JPanel contentPane;
	public JTextField txtNombre;
	public JTextField txtRif;
	public JTextField txtDireccion;
	public JTextField txtWeb;
	public JFrame FrInicio;
	public JButton btnRegistrar;
	public JButton btnAgregarCorreos;
	public JButton btnRegresar;
	public JButton btnAgregarTelefonos;
	public JButton btnAgregarRedes;
	public JDateChooser calendar;

	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 * @throws ParseException 
	 */
	public ProveedorCreateView()  {
		initializerView();
		
	}
	public void initializerView(){
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Nuevo Proveedor");
		FrInicio.setBounds(100, 100, 557, 344);
		FrInicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(12, 12, 512, 332);
		contentPane.setLayout(null);
		
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(22, 275, 117, 25);
		contentPane.add(btnRegistrar);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(22, 27, 70, 15);
		contentPane.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setText("nombre");
		txtNombre.setBounds(22, 44, 114, 19);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblRif = new JLabel("Rif");
		lblRif.setBounds(22, 68, 70, 15);
		contentPane.add(lblRif);
		
		txtRif = new JTextField();
		txtRif.setText("rif");
		txtRif.setBounds(22, 87, 114, 19);
		contentPane.add(txtRif);
		txtRif.setColumns(10);
		
		JLabel lblDireccin = new JLabel("Dirección");
		lblDireccin.setBounds(22, 108, 70, 15);
		contentPane.add(lblDireccin);
		
		txtDireccion = new JTextField();
		txtDireccion.setText("direccion");
		txtDireccion.setBounds(22, 128, 114, 19);
		contentPane.add(txtDireccion);
		txtDireccion.setColumns(10);
		
		JLabel lblPaginaWeb = new JLabel("Pagina Web");
		lblPaginaWeb.setBounds(22, 150, 101, 15);
		contentPane.add(lblPaginaWeb);
		
		txtWeb = new JTextField();
		txtWeb.setText("pag");
		txtWeb.setBounds(22, 168, 167, 19);
		contentPane.add(txtWeb);
		txtWeb.setColumns(10);
		
	    btnRegresar = new JButton("Regresar");
		btnRegresar.setBounds(19, 238, 120, 25);
		contentPane.add(btnRegresar);
		
		btnAgregarTelefonos = new JButton("Agregar Telefonos");
		btnAgregarTelefonos.setBounds(237, 41, 192, 25);
		contentPane.add(btnAgregarTelefonos);
		
		btnAgregarCorreos = new JButton("Agregar Correos");
		btnAgregarCorreos.setBounds(237, 84, 192, 25);
		contentPane.add(btnAgregarCorreos);
		
		btnAgregarRedes = new JButton("Agregar Redes");
		btnAgregarRedes.setBounds(237, 125, 192, 25);
		contentPane.add(btnAgregarRedes);
				
		calendar = new JDateChooser();
		calendar.setBounds(265, 168, 120, 19);
		contentPane.add(calendar);
		
		FrInicio.getContentPane().add(contentPane);
		

		FrInicio.setResizable(true);
		
	}
}
